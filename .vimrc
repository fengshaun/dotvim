set nocompatible
"let base16colorspace="256"
"let g:base16_shell_path="/home/armin/pkgs/base16-builder/output/shell/"
"set t_Co=256

" pathogen
runtime bundle/pathogen/autoload/pathogen.vim
let g:pathogen_disabled = ["coffeescript", "rust", "sparkup", "nerdtree", "snipmate", "syntastic", "gentoo"]
call pathogen#infect()

" indentation
set tabstop=4
set shiftwidth=4
set shiftround
set expandtab
set smarttab

"set termguicolors
set t_8f=[38;2;%lu;%lu;%lum
set t_8b=[48;2;%lu;%lu;%lum

set autoindent
set smartindent

" searching
set ignorecase
set smartcase
set nohlsearch

" buffers
set hidden

" folding
set foldmethod=marker

" gui stuff / colorschemes, fonts ,etc.
set background=dark
colorscheme gruvbox

if has('gui_win32')
  set guifont=Consolas:h10
elseif has('gui_running')
  set guifont=Source\ Code\ Pro\ for\ Powerline\ Semibold\ 11
endif

set guioptions=m
set winminheight=0
set winminwidth=0
set guicursor+=a:blinkon0
set backspace=indent,eol,start
set number

" line width (80 characters):h10
"set colorcolumn=80
"highlight link ColorColumn Comment

" backup
set nobackup
set noswapfile

" filetype and syntax
syntax enable
filetype on
filetype plugin on

" status line
set laststatus=2
"set statusline=[%n:%t%R%H%W]\ %y\ %=\ [%l:%02c/%L]

" status line colors
" hi link StatusLine Normal etc.
"highlight clear StatusLine
"highlight clear StatusLineNC
"highlight StatusLine term=reverse cterm=bold ctermfg=white gui=bold guifg=white
"highlight StatusLineNC ctermfg=DarkGray   guifg=DarkGray

" Pmenu colors
highlight clear Pmenu
highlight clear PmenuSel
highlight link Pmenu Normal
highlight PmenuSel term=reverse cterm=reverse gui=reverse
"highlight Pmenu ctermfg=LightGray guifg=LightGray
"highlight link PmenuSel ErrorMsg

" mappings
let mapleader=","

nmap ; :
nmap gn :tabnew<CR>
nmap <leader>" :split<CR>
nmap <leader>% :vsplit<CR>

noremap <silent> j gj
noremap <silent> k gk
xnoremap <silent> j gj
xnoremap <silent> k gk
noremap <silent> 0 g0
noremap <silent> $ g$

" EasyMotion plugin
let g:EasyMotion_do_mapping = 1
let g:EasyMotion_leader_key = '<leader>.'
let g:EasyMotion_keys = 'aoeuidhtns' " dvorak-friendly
let g:EasyMotion_grouping = 1

" Fuzzy Finder bindings
nnoremap <silent> <leader>f :FufFile<CR>
nnoremap <silent> <leader>b :FufBuffer<CR>
nnoremap <silent> <leader>t :FufCoverageFile<CR>
nnoremap <silent> <leader>n :FufBufferTag<CR>

" Airline
let g:airline_powerline_fonts = 1

" Sparkup
let g:sparkupExecuteMapping = '<c-e>'
let g:sparkupNextMapping = '<c-n>'

" Nerd Commenter plugin default bindings
" nnoremap <leader>cc " comment the line
" nnoremap <leader>cn " same as cc, but forces nesting
" nnoremap <leader>cu " uncomment the line
" nnoremap <leader>c<space> " toggle comment
" nnoremap <leader>cm " minimal comment
" nnoremap <leader>ci " invert comment of individual lines
" nnoremap <leader>cs " sexy comment
" nnoremap <leader>c$ " comment from cursor to end of line

" copy/paste (making it global)
nmap <leader>y "+y
xmap <leader>y "+y
nmap <leader>yy "+yy
nmap <leader>p "+gP

" turn off highlighting after search
nmap <silent> <C-N> :silent noh<CR>

" easy window navigation
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-h> <C-w>h
map <C-l> <C-w>l

" disable arrow keys
map <up> <nop>
map <down> <nop>
map <right> <nop>
map <left> <nop>

" automatic commands
autocmd BufRead,BufNewFile *.txt set ft=text
autocmd BufRead,BufNewFile *.ino setlocal ft=arduino

